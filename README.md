# gmp-sre

這個專案的建立是為了讓管理者能夠了解不同角色在雲專案的任務（或權利）。這裡是以GCP CE常用的Argolis.COM的專案為範例。在Argolis.COM這個Organization底下，有預設的Organization Policies，因此我們設計了五種角色分別置於對應的檔案夾(Folder)中，GCP_Admin, Network_Admin, Cluster_Admin (包含GKE-Cluster or Anthos Cluster兩個Folders)，Developers, 以及Data Scientists供大家參考：
## 內容說明
### GCP_Admin
這個角色可以創建位於Argolis的Project(專案），並使用所賦予的權限解鎖預設開啟的Organization Policy，目前會建立兩個專案，Host Project & Service Project，用於Shared_VPC使用。詳細說明請見[gcp-admin/README.md](gcp-admin/README.md)。

### Network_Admin
這個角色會在GCP_Admin所做出的兩個專案上，建置一層Shared VPC，並發布給Permission對應的Service Project使用。同時也會提供Service Project專案會使用到Host Project VPC的IAM角色對應的防火牆權限(e.g. GKE的ServiceAccount需要本權限來自動開啟LoadBalancer防火牆)。詳細說明請看[network-admin/README.md](network-admin/README.md)


### Platform_Admin:

底下包含三個Folders都是給Platform Admin使用

#### Anthos-BM 檔案夾 (Anthos BareMetal on GCP):
* 部署網路：Shared VPC
* 部署模式：使用GCE VM並安裝Anthos BM 1.14.1版本
* 額外說明：[platform-admin/anthos-bm/README.md](platform-admin/anthos-bm/README.md)
#### GKE-Cluster 檔案夾（GKE on Shared_VPC）: 
* 部署網路：Shared VPC
* 部署模式：Private GKE Cluster
* 額外說明：[platform-admin/gke-cluster/README.md](platform-admin/gke-cluster/README.md)
#### GKE-AutoPilot檔案夾 (GKE AutoPilot on)
* 部署網路：Default VPC
* 部署模式：Public GKE AutoPilot Cluster
* 額外說明: N/A

### Developer: 
這個Folder所存放的是開發者的Sample Code，以及開發者部署對應服務時，可能會一併發布的監控CRDs或是Sidecar Agent。

### Data Scientist:
這個Folder想放的會是資料科學家所部署的Vertex Pipeline程式。
