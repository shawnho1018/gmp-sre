# 專案創建 (Argolis as an example)

## 本專案目的?
There are some organization policies which should be modified to better utilize argolis. I therefore create this project to increase the efficiencies. The creation includes one host project and one service project.  

## 預先確認：
由於gcp-admin使用較高的權限，這個使用者必須在Organization Level 具有以下權限：
* Access Context Manager Admin
* Billing Account User
* Folder Admin
* Organization Administrator
* Organization Policy Administrator
* Organization Role Administrator
* Owner
* Project Creator
* Security Center Admin
* Support Account Administrator
* Tag Administrator

## 如何使用本專案？
* Download this folder into your workstation with terraform CLI. Login with your administrator account on your workstation with the following command.
```
gcloud auth application-default login
```
* Modify the project_id in terraform.tfvars to the project_name your desired. Noted that the current project name and project_id are identical. 
* terraform init && terraform apply 
* After the project is created, you could add your google.com account using Cloud Console.
* Relogin with your google.com account.

## Note
如果設定IAM權限時，有看到Permission Denied的訊息，請等15秒後再直接跑一次(Project Owner權限尚未生效)
```
terraform apply -auto-approve
```
