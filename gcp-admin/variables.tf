variable host_project_id {
    type = string
    description = "Project Name"
}
variable project_id {
    type = string
    description = "Project Name"
}
variable billing_account {
    type = string
}
variable org_id {
    type = string
}

variable region {
    type = string
    default = "asia-east1"
}

variable project_owner_ldap {
    type = string
}