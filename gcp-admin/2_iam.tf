resource "google_service_account" "anthos-admin" {
  for_each = local.projects
  project    = each.value.id
  account_id   = "anthos-admin"
  display_name = "A service account for anthos-admin"
}
resource "google_project_iam_binding" "project" {
  for_each = local.projects
  project    = each.value.id
  role    = "roles/owner"
  members = [
    google_service_account.anthos-admin[each.key].member
  ]
}
resource "google_project_iam_member" "project-owner" {
  for_each = local.projects
  project    = each.value.id
  role    = "roles/editor"
  member = "user:${var.project_owner_ldap}"
}
resource "google_service_account" "postgresql-tester" {
  project    = var.project_id
  account_id   = "postgresql-tester"
  display_name = "A service account for postgresql"
}
resource "google_project_iam_binding" "project-postgresql" {
  project    = var.project_id
  role    = "roles/cloudsql.editor"
  members = [
    google_service_account.postgresql-tester.member
  ]
}