locals {
  projects = {
    "host_project" = {"name" = var.host_project_id, "id" = var.host_project_id}
    "service_project" = {"name" = var.project_id, "id" = var.project_id}
  }
}
resource "google_project" "project" {
  for_each = local.projects
  name       = each.value.name
  project_id = each.value.id
  org_id     = var.org_id
  billing_account = var.billing_account
}

resource "google_project_organization_policy" "domain_restrict_sharing" {
  for_each = local.projects
  project    = each.value.id
  constraint = "iam.allowedPolicyMemberDomains"

  list_policy {
    allow {
      all = true
    }
  }
}

resource "google_project_organization_policy" "vm_extip_policy" {
  for_each = local.projects
  project    = each.value.id
  constraint = "compute.vmExternalIpAccess"

  list_policy {
    allow {
      all = true
    }
  }
}
resource "google_project_organization_policy" "disable_key_creation" {
  for_each = local.projects
  project    = each.value.id
  constraint = "iam.disableServiceAccountKeyCreation"

  boolean_policy {
    enforced = false
  }
}
resource "google_project_organization_policy" "vm_ipforward_policy" {
  for_each = local.projects
  project    = each.value.id
  constraint = "compute.vmCanIpForward"

  list_policy {
    allow {
      all = true
    }
  }
}

resource "google_project_organization_policy" "vm_shield_policy" {
  for_each = local.projects
  project    = each.value.id
  constraint = "compute.requireShieldedVm"

  boolean_policy {
    enforced = false
  }
}

resource "google_project_organization_policy" "require_os_login" {
  for_each = local.projects
  project    = each.value.id
  constraint = "compute.requireOsLogin"

  boolean_policy {
    enforced = false
  }
}
module "host-project-services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  project_id  = local.projects["host_project"].id
  disable_services_on_destroy = false
  activate_apis = [
    "servicenetworking.googleapis.com"
  ]
}
module "project-services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  project_id  = local.projects["service_project"].id
  disable_services_on_destroy = false
  activate_apis = [
    "alloydb.googleapis.com",
    "anthos.googleapis.com",
    "clouddeploy.googleapis.com",
    "cloudscheduler.googleapis.com",
    "clouddebugger.googleapis.com",
    "cloudresourcemanager.googleapis.com",
    "clouderrorreporting.googleapis.com",
    "cloudtrace.googleapis.com",
    "compute.googleapis.com",
    "container.googleapis.com",
    "dns.googleapis.com",
    "gkeconnect.googleapis.com",
    "gkehub.googleapis.com",
    "iam.googleapis.com",
    "iamcredentials.googleapis.com",
    "logging.googleapis.com",
    "meshca.googleapis.com",
    "meshtelemetry.googleapis.com",
    "meshconfig.googleapis.com",
    "monitoring.googleapis.com",
    "servicenetworking.googleapis.com",
    "stackdriver.googleapis.com",
    "osconfig.googleapis.com",
    "vpcaccess.googleapis.com",
    "workstations.googleapis.com"
  ]
}
