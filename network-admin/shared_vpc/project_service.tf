locals {
  projects = {
    "host_project" = {"id" = var.host_project_id}
    "service_project" = {"id" = var.project_id}
  }
}

module "project-services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  for_each = local.projects
  project_id  = each.value.id
  disable_services_on_destroy = false
  activate_apis = [
    "servicenetworking.googleapis.com",
    "vpcaccess.googleapis.com",
    "dns.googleapis.com",
    "cloudtrace.googleapis.com",
    "container.googleapis.com",
    "compute.googleapis.com",
    "iam.googleapis.com",
    "iamcredentials.googleapis.com",
    "logging.googleapis.com",
    "monitoring.googleapis.com",
    "stackdriver.googleapis.com"
  ]
}