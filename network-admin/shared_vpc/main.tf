data "google_project" "project" {
  project_id = var.project_id
}

locals {
  projects = {
    "host_project" = {"id" = var.host_project_id}
    "service_project" = {"id" = var.project_id}
  }
}

resource "google_compute_shared_vpc_host_project" "host_project" {
  project = var.host_project_id
}

resource "google_compute_shared_vpc_service_project" "service" {
  host_project    = google_compute_shared_vpc_host_project.host_project.id
  service_project = var.project_id
}

resource "google_compute_network" "cluster-hybrid-network" {
  name = var.network
  project = google_compute_shared_vpc_host_project.host_project.id
  routing_mode            = "GLOBAL"
  auto_create_subnetworks = false
}

resource "google_compute_subnetwork" "cluster-hybrid-subnetwork" {
  project       = google_compute_shared_vpc_host_project.host_project.id
  name          = "${var.network}"
  ip_cidr_range = var.subnetwork_range
  network       = google_compute_network.cluster-hybrid-network.id
  private_ip_google_access = true
  region          = var.region
  secondary_ip_range {
    range_name    = "pod-range"
    ip_cidr_range = "192.168.0.0/23"
  }
  secondary_ip_range {
    range_name    = "service-range"
    ip_cidr_range = "192.168.128.0/24"
  }
}

resource "google_compute_router" "nat-router" {
  project = google_compute_shared_vpc_host_project.host_project.id
  name    = "${var.network}-router1"
  network = google_compute_network.cluster-hybrid-network.name
  region  = google_compute_subnetwork.cluster-hybrid-subnetwork.region
}

resource "google_compute_router_nat" "nat" {
  name                               = "intranet-snat"
  project                            = google_compute_shared_vpc_host_project.host_project.id
  router                             = google_compute_router.nat-router.name
  region                             = google_compute_router.nat-router.region
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"

  log_config {
    enable = true
    filter = "ERRORS_ONLY"
  }
}

resource "google_project_iam_binding" "hostServiceUser" {
  project = google_compute_shared_vpc_host_project.host_project.id
  role    = "roles/container.hostServiceAgentUser"

  members = [
    "serviceAccount:service-${data.google_project.project.number}@container-engine-robot.iam.gserviceaccount.com",
    "serviceAccount:anthos-admin@${var.project_id}.iam.gserviceaccount.com"
  ]
}

resource "google_project_iam_binding" "networkAdmin" {
  project = google_compute_shared_vpc_host_project.host_project.id
  role    = "roles/compute.networkAdmin"

  members = [
    "user:${var.user_ldap}",
  ]
}

resource "google_project_iam_binding" "alloyDBAdmin" {
  project = google_compute_shared_vpc_service_project.service.id
  role    = "roles/alloydb.admin"

  members = [
    "user:${var.user_ldap}",
  ]
}

resource "google_project_iam_binding" "securityAdmin" {
  project = google_compute_shared_vpc_host_project.host_project.id
  role    = "roles/compute.securityAdmin"

  members = [
    "serviceAccount:service-${data.google_project.project.number}@container-engine-robot.iam.gserviceaccount.com",
    "serviceAccount:anthos-admin@${var.project_id}.iam.gserviceaccount.com"
  ]
}
resource "google_project_iam_binding" "networkUser" {
  project = google_compute_shared_vpc_host_project.host_project.id
  role    = "roles/compute.networkUser"

  members = [
    "serviceAccount:service-${data.google_project.project.number}@container-engine-robot.iam.gserviceaccount.com",
    "serviceAccount:${data.google_project.project.number}@cloudservices.gserviceaccount.com",
    "user:${var.user_ldap}",
    "serviceAccount:anthos-admin@${var.project_id}.iam.gserviceaccount.com"
  ]
}
