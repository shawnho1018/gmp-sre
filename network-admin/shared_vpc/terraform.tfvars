# This project used to create VPC and its subnetwork in the host and service project
# project_id: 用來部署GKE的Service Project
project_id                    = "shawnho-demo-2023"
# host_project_id: 用來部署Shared VPC的專案
host_project_id               = "shawnho-netadmin-2023"
# 部署於host_project但會分享給service project的VPC與subnet名稱 （勿更換）
network                       = "simulated"
subnetwork                    = "simulated"
# Subnetwork會被展開的區域
region                        = "asia-east1"
zone                          = "asia-east1-a"

# User LDAP (User Account which used to grant permission)
user_ldap                     = "shawnho@google.com"
