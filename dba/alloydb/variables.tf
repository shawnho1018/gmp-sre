variable project_id {
    type    = string
    default = "my-project-id"
}

variable host_project_id {
    type    = string
    default = "host-vpc-project-id"
}

variable region {
    type    = string
    default = "asia-east1"
}

variable network_name {
    type    = string
    default = "simulated"
}