# These parameters were used to create Anthos Cluster using GCE. 
# 請注意在執行這個Terraform Script前，請先確認已經使用Shared VPC腳本先製作出一個VPC與Subnetwork，稱為simulated
project_id                    = "shawnho-demo-2023"
host_project_id               = "shawnho-netadmin-2023"
region                        = "asia-east1"
zone                          = "asia-east1-a"
# GCE安裝使用的服務帳號。本服務帳號若有執行GCP Admin的Terraform Script，會被自行創建。若使用者直接使用本目錄創建Anthos Cluster，則本服務帳號需手動建置，並設定對應的IAM權限。這個帳號建議給予Project Editor的權限，用以存取所需的container registry等資源。
# 服務帳號對應的格式為：[service-account]@[project-id].iam.gserviceaccount.com
gce_vm_service_account        = "anthos-admin@shawnho-demo-2023.iam.gserviceaccount.com"
# 服務帳號對應的Key File的本機存放位置，可下載上述服務帳號的Key
credentials_file              = "/Users/shawnho/gcp-keys/shawnho-demo-2023-sa.key"
# 決定Anthos Cluster的Master Node (controlplane) + Worker Node (worker)數量
instance_count                = {
                                  "controlplane" : 1
                                  "worker" : 1
                                }
############################################################################################
network                       = "simulated"
subnetwork                    = "simulated"
mode                          = "install"
#mode                          = "manuallb"
abm_cluster_id                = "cluster1"
anthos_service_account_name   = "baremetal-gcr"
resources_path                = "./resources"
machine_type                  = "n1-standard-8"
abm_version                   = "1.16.3"


