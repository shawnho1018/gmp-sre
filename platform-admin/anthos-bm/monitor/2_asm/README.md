# Service Mesh & Topology & Trace Configuration
This project would install anthos service mesh (1.16.2) and utilize the corresponding kiali and jaeger to integrate with GMP. After the deployment, we'll also configure istio-prometheus-scraper and istio-sidecar (envoy-agent) scraper to allow prometheus to scrape the topology information. We'll also deploy a hipster microservice to demonstrate the topology and trace worked. 

## Modifications
There are couple changes mainly in kaili.yaml. I mainly add configuration in externa_service to config istio, prometheus, grafana, and jaeger (Tracing) as shown below. We use yq to modify those configurations into the original kiali.yaml
```
   external_services:
      custom_dashboards:
        enabled: true
      istio:
        config_map_name: "istio-${ASM_LABEL}"
        istio_sidecar_injector_config_map_name: "istio-sidecar-injector-${ASM_LABEL}"
        istiod_deployment_name: "istiod-${ASM_LABEL}"
        root_namespace: istio-system
      istio_labels:
        app_label_name: "app"
        version_label_name: "version"
      prometheus:
        url: ${PROMETHEUS_SERVICE}
      grafana:
        enabled: true
        in_cluster_url: ${GRAFANA_SERVICE}
      tracing:
        enabled: true
        in_cluster_url: 'http://tracing.istio-system.svc:16685/jaeger'
        use_grpc: true
```