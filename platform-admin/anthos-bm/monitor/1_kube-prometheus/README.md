 ahead of the upstream repository.
README.md
1.85 KiB
# Kube-Prometheus Helm Operator
This module utilize kube-prometheus helm to install the self-deployed GMP. Install script would download helm chart as well as install the kube-prometheus in the cluster. Since we're using GMP whose service account requires metrics.write permission, script would also help to configure an IAM and set up workload identity for GMP's kubernetes service account. 
## Modifications:
There are three modifications I made for Anthos bare metal cluster compared to the original values.yaml:
1. Change PrometheusSpec as follows
```
    image:
      registry: gke.gcr.io
      repository: prometheus-engine/prometheus
      tag: v2.35.0-gmp.2-gke.0
      sha: ""
```
2. Modify prometheus.serviceMonitorSelectorNilUsesHelmValues & prometheus.podMonitorSelectorNilUsesHelmValues to false. This allows users to deploy their own serviceMonitor & podMonitor in the cluster but still remains the original serviceMonitor introduced by kube-prometheus stack. 
```
    serviceMonitorSelectorNilUsesHelmValues: false
    podMonitorSelectorNilUsesHelmValues: false
```

3. Change Prometheus Node Exporter to Port 9101 since there is node exporter already installed in Anthos cluster. (but cannot be scrapped because lack of certificates.)
```
prometheus-node-exporter:
  namespaceOverride: ""
  podLabels:
    ## Add the 'node-exporter' label to be used by serviceMonitor to match standard common usage in rules and grafana dashboards
    ##
    jobLabel: node-exporter
  releaseLabel: true
  extraArgs:
    - --collector.filesystem.mount-points-exclude=^/(dev|proc|sys|var/lib/docker/.+|var/lib/kubelet/.+)($|/)
    - --collector.filesystem.fs-types-exclude=^(autofs|binfmt_misc|bpf|cgroup2?|configfs|debugfs|devpts|devtmpfs|fusectl|hugetlbfs|iso9660|mqueue|nsfs|overlay|proc|procfs|pstore|rpc_pipefs|securityfs|selinuxfs|squashfs|sysfs|tracefs)$
  service:
    portName: http-metrics
    port: 9101
    targetPort: 9101
```