#!/bin/bash
source ../env.sh
function __main__ () {
  __install_helm__
  __download_kube_prometheus__
  __create_writeonly_sa__
  __install_kube_prometheus__  
  __config_workload_identity__
}

function __install_helm__ () {
  curl https://baltocdn.com/helm/signing.asc | gpg --dearmor | sudo tee /usr/share/keyrings/helm.gpg > /dev/null
  sudo apt-get install apt-transport-https --yes
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/helm.gpg] https://baltocdn.com/helm/stable/debian/ all main" | sudo tee /etc/apt/sources.list.d/helm-stable-debian.list
  sudo apt-get update
  sudo apt-get install -y helm
}

function __install_kube_prometheus__ () {
  kubectl create ns ${GMP_NAMESPACE}
  helm install prometheus -n ${GMP_NAMESPACE} -f ./values.yaml kube-prometheus-stack/
}

function __download_kube_prometheus__ () {
  helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
  helm pull prometheus-community/kube-prometheus-stack --version 45.4.0
  tar zxvf kube-prometheus-stack-45.4.0.tgz
  rm -f kube-prometheus-stack-45.4.0.tgz
}

function __create_writeonly_sa__ () {
    gcloud iam service-accounts create ${GMP_SERVICEACCOUNT}

    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member=serviceAccount:${GMP_SERVICEACCOUNT}@${PROJECT_ID}.iam.gserviceaccount.com \
    --role=roles/monitoring.metricWriter
}
function __config_workload_identity__ () {
    echo "config workload identity..."
    gcloud iam service-accounts add-iam-policy-binding \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[${GMP_NAMESPACE}/prometheus-kube-prometheus-prometheus]" \
    ${GMP_SERVICEACCOUNT}@${PROJECT_ID}.iam.gserviceaccount.com

    kubectl annotate serviceaccount --namespace ${GMP_NAMESPACE} prometheus-kube-prometheus-prometheus \
    iam.gke.io/gcp-service-account=${GMP_SERVICEACCOUNT}@${PROJECT_ID}.iam.gserviceaccount.com
}
function __upgrade_prometheus__ () {
    helm upgrade prometheus -n ${GMP_NAMESPACE} -f ./values.yaml .
}
function __delete_workload_identity__ () {
    echo "delete workload identity..."
    gcloud iam service-accounts remove-iam-policy-binding ${GMP_SERVICEACCOUNT}@${PROJECT_ID}.iam.gserviceaccount.com \
    --role roles/iam.workloadIdentityUser \
    --member "serviceAccount:${PROJECT_ID}.svc.id.goog[${GMP_NAMESPACE}/prometheus-kube-prometheus-prometheus]"

    kubectl annotate serviceaccount prometheus-kube-prometheus-prometheus -n ${GMP_NAMESPACE} iam.gke.io/gcp-service-account-
}

function __print_separator__ () {
  echo "------------------------------------------------------------------------------"
}

# Run the script from main()
__main__ "$@"