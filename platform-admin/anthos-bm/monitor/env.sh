#!/bin/bash
# 參數說明
export ROOT=$(pwd)
# GMP所對應使用的Project IAM service-account。會在GMP安裝時一併產生
export GMP_SERVICEACCOUNT="gmp-test-sa"
# GMP預設安裝的Namespace位置
export GMP_NAMESPACE="monitoring"
# 註冊Anthos Cluster到Anthos Fleet的ID名稱，通常與Project_ID相同
export PROJECT_ID="shawnho-demo-2023"
# Get Project_Number
export PROJECT_NUMBER=$(gcloud projects describe $PROJECT_ID --format="value(projectNumber)")
export FLEET_ID=${PROJECT_ID}
# 在Connect Gateway時，希望設定為Cluster Administrator的使用者帳號
export USER="shawnho@google.com"
# 安裝ASM時，中間產物的存放目錄
export ASM_DIR_PATH="./asm-install"
# ASM用來指定Namespace Auto-Injection的Label名稱
export ASM_LABEL="asm-1182-0"
# ASM安裝版本（用以取出kiali與Jaeger的yaml)
export ASM_VERSION="1.18.2-asm.0"
# 預設kubeconfig在跳板機預設存放的路徑（此路徑需手動由bmctl-workspace/cluster1/cluster1-kubeconfig 拷貝到~/.kube/config中）
export KUBECONFIG_PATH="/home/tfadmin/.kube/config"
# 設定Kiali時，對應的GMP Service DNS
export PROMETHEUS_SERVICE="http://prometheus-kube-prometheus-prometheus.monitoring.svc.cluster.local:9090"
# 設定Kiali時，對應的Grafana Service DNS
export GRAFANA_SERVICE="http://prometheus-grafana.monitoring.svc.cluster.local:80"

function __check_exit_status__ () {
  EXIT_CODE=$1
  SUCCESS_MSG=$2
  FAILURE_MSG=$3

  if [ "$EXIT_CODE" -eq 0 ]
  then
    echo "$SUCCESS_MSG"
  else
    echo "$FAILURE_MSG" >&2
    exit "$EXIT_CODE"
  fi
}