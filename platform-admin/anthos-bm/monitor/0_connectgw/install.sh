#!/bin/bash
source ../env.sh

# Enable API
gcloud services enable --project=${PROJECT_ID}  \
    connectgateway.googleapis.com \
    anthos.googleapis.com \
    gkeconnect.googleapis.com \
    gkehub.googleapis.com \
    cloudresourcemanager.googleapis.com

# Setup IAM
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
   --member user:${USER} \
   --role='roles/serviceusage.serviceUsageAdmin'
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
   --member user:${USER} \
   --role='roles/resourcemanager.projectIamAdmin'

gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member=user:${USER} \
    --role=roles/gkehub.gatewayAdmin
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member=serviceAccount:${PROJECT_NUMBER}@cloudbuild.gserviceaccount.com \
    --role=roles/gkehub.gatewayAdmin
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member=user:${USER} \
    --role=roles/gkehub.viewer
gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member=serviceAccount:${PROJECT_NUMBER}@cloudbuild.gserviceaccount.com \
    --role=roles/gkehub.viewer
# Upgrade gcloud CLI
sudo snap remove google-cloud-sdk && sudo snap install google-cloud-cli --classic

gcloud container fleet memberships generate-gateway-rbac  \
    --membership=cluster1 \
    --role=clusterrole/cluster-admin \
    --users=${USER},${PROJECT_NUMBER}@cloudbuild.gserviceaccount.com \
    --project=${PROJECT_ID} \
    --kubeconfig=${KUBECONFIG_PATH} \
    --context=cluster1-admin@cluster1 \
    --apply