resource "kubernetes_namespace" "hipster" {
  metadata {
    labels = {
      "service" = "hipster"
      "istio.io/rev" = "asm-managed"
    }
    name = "hipster"
  }
}

module "hipster" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"

  project_id              = data.google_project.project.project_id
  cluster_name            = module.gke.name
  cluster_location        = module.gke.location
  module_depends_on       = [module.gke]
  kubectl_create_command  = "kubectl apply -f hipster.yaml -n ${kubernetes_namespace.hipster.id} && kubectl apply -f istio-hipster.yaml -n ${kubernetes_namespace.hipster.id}"
  kubectl_destroy_command = "echo done"
}