module "fleet" {
  source     = "terraform-google-modules/kubernetes-engine/google//modules/fleet-membership"
  project_id = data.google_project.project.project_id
  location   = module.gke.location
  cluster_name = module.gke.name
  enable_fleet_registration = true
}

resource "google_gke_hub_feature" "feature" {
  name = "fleetobservability"
  location = "global"
  depends_on = [module.fleet]
  spec {
    fleetobservability {
      logging_config {
        default_config {
          mode = "COPY"
        }
        fleet_scope_logs_config {
          mode = "MOVE"
        }
      }
    }
  }
}