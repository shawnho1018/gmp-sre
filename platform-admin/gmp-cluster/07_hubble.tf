resource "terraform_data" "dataplane_v2_observability" {
  # Defines when the provisioner should be executed
  triggers_replace = [
    module.gke.name
  ]

  provisioner "local-exec" {
    command = "${path.module}/install-hubbleui.sh"
    environment = {
        REGION  = var.region
        CLUSTER_NAME = module.gke.name
    }
  }
}

module "hubble_ui" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"

  project_id              = data.google_project.project.project_id
  cluster_name            = module.gke.name
  cluster_location        = module.gke.location
  module_depends_on       = [terraform_data.dataplane_v2_observability]
  kubectl_create_command  = "kubectl apply -f hubble/"
  kubectl_destroy_command = "echo done"
}