
# Deploy Cert-Manager & Config VPC firewall for GKE master
resource "google_compute_firewall" "cert-manager-firewall-rule" {
  project = data.google_project.vpc_host_project.name
  name = "cert-manager-${module.gke.name}"
  network       = data.google_compute_network.host_network.name
  target_tags   = [module.gke.name]
  source_ranges = [var.master_ipv4_cidr_block]
  direction     = "INGRESS"

  allow {
    protocol = "tcp"
    ports    = [9443]
  }  
}

data "google_service_account" "otel-collector" {
  account_id   = "otel-collector"
}

resource "helm_release" "cert-manager" {
  name       = "jetstack"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  create_namespace = true
  namespace  = "cert-manager"
  set {
    name  = "installCRDs"
    value = "true"
  }
  set {
    name = "global\\.leaderElection\\.namespace"
    value = "cert-manager"
  }
  set {
    name = "extraArgs"
    value = "{--issuer-ambient-credentials=true}"
  }
}
# Deploy Otel-Operator
module "otel-operator" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"
  cluster_name            = module.gke.name
  cluster_location        = module.gke.location
  kubectl_create_command  = "kubectl apply -f opentelemetry/otel-operator.yaml"
  kubectl_destroy_command = "echo done"
  project_id              = data.google_project.project.project_id
  module_depends_on       = [helm_release.cert-manager]
}
# Deploy Otel-Collector
resource "kubernetes_namespace" "otel-collector" {
  metadata {
    labels = {
      "service" = "otel-collector"
    }
    name = "otel-collector"
  }
}
module "otel-collector" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"

  project_id              = data.google_project.project.project_id
  cluster_name            = module.gke.name
  cluster_location        = module.gke.location
  kubectl_create_command  = "kubectl apply -f opentelemetry/otel-sa.yaml && kubectl apply -f opentelemetry/otel-collector.yaml"
  kubectl_destroy_command = "echo done"
  module_depends_on       = [module.otel-operator.wait]
}

module "my-app-workload-identity" {
  source              = "terraform-google-modules/kubernetes-engine/google//modules/workload-identity"
  use_existing_gcp_sa = true
  name                = data.google_service_account.otel-collector.account_id
  project_id          = data.google_project.project.project_id
  use_existing_k8s_sa = true
  cluster_name        = module.gke.name
  location            = module.gke.location
  k8s_sa_name         = "default"
  namespace           = kubernetes_namespace.otel-collector.id
}
