resource "helm_release" "cross-plane" {
  name       = "crossplane"
  repository = "https://charts.crossplane.io/stable"
  chart      = "crossplane"
  namespace   = "crossplane-system"
  create_namespace = true
}

resource "kubernetes_secret" "gcp-secret" {
  metadata {
    name = "gcp-secret"
    namespace = helm_release.cross-plane.namespace
  }
  data = {
    creds = file(var.iac_servicekey_path)
  }
  type = "opaque"
}
### TODO: Test crossplane-provider & crossplane-provider-config 

# module "crossplane-provider" {
#   source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"

#   project_id              = data.google_project.project.project_id
#   cluster_name            = module.gke.name
#   cluster_location        = module.gke.location
#   module_depends_on       = [module.gke]
#   kubectl_create_command  = "kubectl apply -f crossplane/provider.yaml"
#   kubectl_destroy_command = "kubectl delete -f crossplane/provider.yaml"
# }

# module "crossplane-provider-config" {
#   source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"

#   project_id              = data.google_project.project.project_id
#   cluster_name            = module.gke.name
#   cluster_location        = module.gke.location
#   module_depends_on       = [module.crossplane-provider.wait]
#   kubectl_create_command  = "sed 's/PROJECT_ID/${data.google_project.project.project_id}/g' crossplane/provider-config.template > crossplane/provider-config.yaml && kubectl apply -f crossplane/provider-config.yaml"
#   kubectl_destroy_command = "kubectl delete -f crossplane/provider-config.yaml && rm crossplane/provider-config.yaml"
# }
