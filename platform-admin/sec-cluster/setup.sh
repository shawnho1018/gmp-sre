#!/bin/bash
kubectl create ns gateway
kubectl label ns gateway istio.io/rev=asm-managed
kubectl apply -f istio-ingressgateway/ -n gateway
kubectl create ns hipster
kubectl label ns hipster istio.io/rev=asm-managed
kubectl apply -f ./hipster.yaml -n hipster
kubectl apply -f ./istio-hipster.yaml -n hipster
