#!/bin/bash
gcloud container clusters update ${CLUSTER_NAME} \
    --region ${REGION} \
    --enable-dataplane-v2-metrics
gcloud container clusters update ${CLUSTER_NAME} \
    --region ${REGION} \
    --dataplane-v2-observability-mode=INTERNAL_VPC_LB