data "google_compute_network" "host_network" {
  name    = var.network
  project = var.host_project_id
}
data "google_compute_subnetwork" "host_subnetwork" {
  name    = var.subnet
  project = var.host_project_id
  region  = var.region
}

data "google_kms_key_ring" "keyring" {
  name     = "keyring-gke"
  location = "asia-east1"
}

resource "google_kms_crypto_key" "gke-key" {
  name            = "gke-key"
  key_ring        = data.google_kms_key_ring.keyring.id
  rotation_period = "100000s"

  lifecycle {
    prevent_destroy = false
  }
}
resource "google_kms_key_ring_iam_binding" "gke-key" {
  key_ring_id = data.google_kms_key_ring.keyring.id
  role        = "roles/cloudkms.cryptoOperator"

  members = [
    "serviceAccount:service-${data.google_project.project.number}@container-engine-robot.iam.gserviceaccount.com",
  ]
}

module "gke" {
  source                     = "terraform-google-modules/kubernetes-engine/google//modules/private-cluster"
  project_id                 = data.google_project.project.project_id
  name                       = var.cluster_name
  region                     = data.google_compute_subnetwork.host_subnetwork.region
  zones                      = var.zones
  network                    = data.google_compute_network.host_network.name
  subnetwork                 = data.google_compute_subnetwork.host_subnetwork.name
  deletion_protection        = false
  network_project_id         = var.host_project_id
  ip_range_pods              = data.google_compute_subnetwork.host_subnetwork.secondary_ip_range[3].range_name
  ip_range_services          = data.google_compute_subnetwork.host_subnetwork.secondary_ip_range[2].range_name
  http_load_balancing        = true
  horizontal_pod_autoscaling = true
  filestore_csi_driver       = true
  gce_pd_csi_driver          = true
  gcs_fuse_csi_driver        = true
  remove_default_node_pool   = true
  enable_vertical_pod_autoscaling = true
  datapath_provider          = "ADVANCED_DATAPATH"
  enable_binary_authorization = true
  enable_shielded_nodes      = true
  enable_cost_allocation     = true
  enable_network_egress_export = true
  enable_resource_consumption_export = true
  resource_usage_export_dataset_id = "gke_cluster_consumption"
  enable_private_endpoint    = false
  enable_private_nodes       = true
  grant_registry_access      = true
  master_ipv4_cidr_block     = var.master_ipv4_cidr_block
  cluster_dns_provider       = "CLOUD_DNS"
  cluster_dns_scope          = "CLUSTER_SCOPE"
  monitoring_enable_managed_prometheus = true
  gke_backup_agent_config    = true
  monitoring_enabled_components = ["SYSTEM_COMPONENTS", "APISERVER", "CONTROLLER_MANAGER", "SCHEDULER"]
  release_channel            = "REGULAR"
  gateway_api_channel        = "CHANNEL_STANDARD"
  database_encryption        = [{"key_name": google_kms_crypto_key.gke-key.id, "state": "ENCRYPTED"}]
  depends_on                 = [google_kms_key_ring_iam_binding.gke-key]
  node_pools = [
    {
      name                      = "default-node-pool"
      machine_type              = var.vm_type
      node_locations            = "asia-east1-c"
      min_count                 = 1
      max_count                 = 3
      local_ssd_count           = 0
      spot                      = true
      disk_size_gb              = 100
      disk_type                 = "pd-standard"
      image_type                = "COS_CONTAINERD"
      enable_gcfs               = true
      enable_gvnic              = false
      auto_repair               = true
      auto_upgrade              = true
      preemptible               = false
      initial_node_count        = 1
    } 
  ]
  node_pools_oauth_scopes = {
    all = [
      "https://www.googleapis.com/auth/cloud-platform"
    ]
  }

  node_pools_labels = {
    all = {}
    default-node-pool = {
      default-node-pool = true
      spot-instance     = true
      belong-to         = var.cluster_name
    }
  }

  node_pools_metadata = {
    all = {}
    default-node-pool = {
      node-pool-metadata-custom-value = "default-node-pool"
    }
  }

  node_pools_taints = {
    all = []

    default-node-pool = [
      {
        key    = "default-node-pool"
        value  = true
        effect = "PREFER_NO_SCHEDULE"
      },
    ]
  }

  node_pools_tags = {
    all = []
    default-node-pool = [
      "default-node-pool",
    ]
  }
  master_authorized_networks = [
    {
       cidr_block   = "0.0.0.0/0"
       display_name = "VPC"
    } 
  ]
}
