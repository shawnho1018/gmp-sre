# OpenTelemetry in GKE
The current OpenTelemetry collector must use image from "otel/opentelemetry-collector-contrib:0.74.0" to accomodate unique exporters, (e.g. googlemanagedprometheus or gcp) and processors (e.g. transform). 

It implies the opentelemetry collector config cannot be used since the resulting opentelemetry collector would have its image from: ghcr.io/open-telemetry/opentelemetry-collector-releases/opentelemetry-collector:0.83.0.