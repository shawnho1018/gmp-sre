module "managed_gmp" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"

  project_id              = data.google_project.project.project_id
  cluster_name            = module.gke.name
  cluster_location        = module.gke.location
  module_depends_on       = [module.gke]
  kubectl_create_command  = "kubectl apply -f managed-gmp/"
  kubectl_destroy_command = "echo done"
}

module "patch_operatorconfig" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"

  project_id              = data.google_project.project.project_id
  cluster_name            = module.gke.name
  cluster_location        = module.gke.location
  module_depends_on       = [module.managed_gmp] 
  kubectl_create_command  = "kubectl patch operatorconfig config -n gmp-public --patch-file managed-gmp/patch/patch-operatorconfig.json --type=merge"
  kubectl_destroy_command = "echo done"
}
