resource "helm_release" "bindplane" {
  name       = "bindplane"
  repository = "https://observiq.github.io/bindplane-op-helm"
  chart      = "bindplane"
  create_namespace = true
  namespace  = "bindplane"
  reset_values = true
  cleanup_on_fail = true
  set {
    name = "config.username"
    value = "admin"
  }
  set {
    name = "config.password"
    value = "admin"
  }
  set {
    name = "backend.bbolt.storageClass"
    value = "standard"
  }
  set {
      name = "secret_key"
      value = "13c15d7c-233e-49e3-bef4-f7498f33518f"
  }
  set {
      name = "sessions_secret"
      value = "a4e5aa85-5df4-4edd-8cfe-d8a7257d09dd"
  }
}