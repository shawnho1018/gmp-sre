# MemoryStore Demo and Use
This project will automatically create a memorystore and a GCE VM with redis-cli installed. In order to login to memorystore instance, user needs to login to the GCE VM with ssh and then
```
token=$(gcloud auth print-access-token)
redis-cli -h 10.0.0.250 -a $token
Warning: Using a password with '-a' or '-u' option on the command line interface may not be safe.
10.0.0.250:6379> set test 1
OK
10.0.0.250:6379> get test
"1"
```
