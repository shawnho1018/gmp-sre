provider "google" {
  project = var.project_id
  region  = var.region
  credentials = "${file(var.iac_servicekey_path)}"
}

provider "google-beta" {
  project = var.project_id
  region  = var.region
  credentials = "${file(var.iac_servicekey_path)}"
}

module "project-services" {
  source  = "terraform-google-modules/project-factory/google//modules/project_services"
  project_id  = var.project_id
  disable_services_on_destroy = false
  activate_apis = [
    "redis.googleapis.com",
    "compute.googleapis.com",
    "serviceconsumermanagement.googleapis.com",
    "networkconnectivity.googleapis.com"
  ]
}

resource "google_compute_network" "redis-network" {
  name = "redis-test-network"
  project = var.project_id
}
 resource "google_compute_global_address" "service_range" {
   name          = "address"
   purpose       = "VPC_PEERING"
   address_type  = "INTERNAL"
   prefix_length = 16
   network       = google_compute_network.redis-network.id
 }
 resource "google_service_networking_connection" "private_service_connection" {
   network                 = google_compute_network.redis-network.id
   service                 = "servicenetworking.googleapis.com"
   reserved_peering_ranges = [google_compute_global_address.service_range.name]
 }

resource "google_compute_subnetwork" "redis_subnet" {
  name          = "mysubnet"
  ip_cidr_range = "10.0.0.248/29"
  region        = "asia-east1"
  network       = google_compute_network.redis-network.id
}

resource "google_redis_instance" "cache" {
  project        = var.project_id
  name           = var.memorystore01_name
  tier           = "STANDARD_HA"
  memory_size_gb = 1

  location_id             = "asia-east1-a"
  alternative_location_id = "asia-east1-b"

  authorized_network = google_compute_network.redis-network.id
  connect_mode       = "PRIVATE_SERVICE_ACCESS"

  redis_version     = "REDIS_6_X"
  display_name      = "Terraform Test Instance"

  depends_on = [google_service_networking_connection.private_service_connection, module.project-services]

  lifecycle {
    prevent_destroy = false
  }
}

resource "google_compute_instance" "default" {
  name         = "my-instance"
  machine_type = "e2-medium"
  zone         = "asia-east1-a"
  tags = ["client"]
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-11"
      labels = {
        my_label = "value"
      }
    }
  }

  network_interface {
    network = google_compute_network.redis-network.id

    access_config {
      // Ephemeral public IP
    }
  }
  metadata = {
    foo = "bar"
  }

  metadata_startup_script = "apt update && apt install -y redis"

  service_account {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    email  = google_service_account.default.email
    scopes = ["cloud-platform"]
  }
}
resource "google_service_account" "default" {
  account_id   = "redis-sa"
  display_name = "Custom SA for VM Instance"
}
resource "google_project_iam_member" "redis-sa" {
  project = var.project_id
  role    = "roles/redis.admin"
  member  = "serviceAccount:${google_service_account.default.email}"
}

resource "google_project_iam_member" "db-connector" {
  project = var.project_id
  role    = "roles/redis.dbConnectionUser"
  member  = "serviceAccount:${google_service_account.default.email}"
}

resource "google_compute_firewall" "default" {
  name    = "test-firewall"
  network = google_compute_network.redis-network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges = [
    "0.0.0.0/0",
  ]
  target_tags = ["client"]
}
