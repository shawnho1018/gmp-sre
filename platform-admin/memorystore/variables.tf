variable "project_id" {
    type = string 
    description = "Project to store terraform state variables"
}

variable "memorystore01_name" {
    type = string
    description = "name of the memorystore"
}

variable "region" {
    type = string
}
variable "iac_servicekey_path" {
    type = string
}