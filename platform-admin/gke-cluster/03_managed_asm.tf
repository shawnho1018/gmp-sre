module "asm" {
  source            = "terraform-google-modules/kubernetes-engine/google//modules/asm"
  project_id        = data.google_project.project.project_id
  cluster_name      = module.gke.name
  cluster_location  = module.gke.location
#  internal_ip       = true
  fleet_id          = module.fleet.cluster_membership_id 
}

resource "kubernetes_namespace" "gateway" {
  metadata {
    labels = {
      "service" = "gateway",
      "istio.io/rev" = "asm-managed"
    }
    name = "gateway"
  }
}

module "istio-gateway" {
  source = "terraform-google-modules/gcloud/google//modules/kubectl-wrapper"

  project_id              = data.google_project.project.project_id
  cluster_name            = module.gke.name
  cluster_location        = module.gke.location
  module_depends_on       = [module.gke]
  kubectl_create_command  = "kubectl apply -f istio-ingressgateway/ -n ${kubernetes_namespace.gateway.id}"
  kubectl_destroy_command = "kubectl delete -f istio-ingressgateway/  -n ${kubernetes_namespace.gateway.id}"
}