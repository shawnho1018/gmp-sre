resource "google_clouddeploy_target" "dev" {
  location = "asia-east1"
  name     = "gke-cluster-dev"

  description = "GKE Cluster in Asia-east1"

  gke {
    cluster = module.gke.cluster_id
  }

  labels = {
    cluster = module.gke.name
  }

  project          = data.google_project.project.project_id
  require_approval = false
}
resource "google_clouddeploy_target" "prod" {
  location = "asia-east1"
  name     = "gke-cluster-prod"

  description = "GKE Cluster in Asia-east1"

  gke {
    cluster = module.gke.cluster_id
  }

  labels = {
    cluster = module.gke.name
  }

  project          = data.google_project.project.project_id
  require_approval = false
}

resource "google_clouddeploy_delivery_pipeline" "primary" {
  location = var.region
  name     = "pipeline"
  project  = data.google_project.project.project_id
  description = "sample-clouddeploy"
  labels = {
    app = "prober"
  }
  annotations = {
    app = "prober"
  }

  serial_pipeline {
    stages {
      profiles  = ["dev"]
      target_id = google_clouddeploy_target.dev.target_id
    }

    stages {
      profiles  = ["prod"]
      target_id = google_clouddeploy_target.prod.target_id
    }
  }
  provider = google-beta
}