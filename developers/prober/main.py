from flask import Flask, request, render_template

app = Flask(__name__, static_url_path='', static_folder='gen-ai-ui/src')

@app.route("/client-ip")
def index():
  ip = request.remote_addr
  return f"Demo Site's IP address is: {ip}"
@app.route('/client-env-ip')
def client():
    ip_addr = request.environ['REMOTE_ADDR']
    return 'Your client IP address is:' + ip_addr

@app.route('/client-env-forward')
def proxy_client():
    ip_addr = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)
    return 'Your X-Forwarded-For IP address is:' + ip_addr
@app.route('/')
def show():
    return render_template('query.html', name="Shawn")

if __name__ == "__main__":
  app.run(host="0.0.0.0", port=8443, debug=True)