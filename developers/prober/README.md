# Prober for Troubleshooting
This project is creating to diagnose source ip of the incoming client. Since there are LoadBalancer in front of the kubernetes pod, client source ip may not be revealed accurately. This project utilizes 3 API calls to check:
1. /client-ip: IP retrieved from request.remote_addr
2. /client-env-ip: IP retrieved from request.environ
3. /client-env-forward: IP retrieved from request.environ's X-Forwarded-For