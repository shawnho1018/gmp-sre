#!/bin/bash
export instance_name="pgvector-demo"
export database_name="retail"
export database_user="retail-admin"
export database_password="postgres"
export region="asia-east1"

# Create the database, if it does not exist.
gcloud sql instances create ${instance_name} --database-version=POSTGRES_15 --region=${region} --cpu=1 --memory=4GB --root-password=${database_password}
gcloud sql databases create ${database_name} --instance=${instance_name}

# Create the database user for accessing the database.
gcloud sql users create ${database_user} \
  --instance=${instance_name} \
  --password=${database_password}