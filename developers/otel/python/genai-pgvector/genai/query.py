from pgvector.asyncpg import register_vector
import asyncio
import asyncpg
from google.cloud.sql.connector import Connector
from langchain.embeddings import VertexAIEmbeddings

project_id = "shawnho-demo-2023"
region = "us-central1"
instance_name = "pgvector-demo"
database_user = "retail-admin"
database_password = "postgres"
database_name = "retail"


matches = []


def imposter_syndrome(type, value, traceback):
    print("I am a bad coder ☹️")
    print(f"{type} of error has occurred, the value: {value}, and you can see traceback: {traceback}")

async def select_toy_byquery(user_query, min_price, max_price):
    import sys
    embeddings_service = VertexAIEmbeddings()
    qe = embeddings_service.embed_query(user_query)
    sys.excepthook = imposter_syndrome

    loop = asyncio.get_running_loop()
    matches = []
    async with Connector(loop=loop) as connector:
        # Create connection to Cloud SQL database.
        conn: asyncpg.Connection = await connector.connect_async(
            f"{project_id}:{region}:{instance_name}",  # Cloud SQL instance connection name
            "asyncpg",
            user=f"{database_user}",
            password=f"{database_password}",
            db=f"{database_name}",
        )

        await register_vector(conn)
        similarity_threshold = 0.7
        num_matches = 5

        # Find similar products to the query using cosine similarity search
        # over all vector embeddings. This new feature is provided by `pgvector`.
        results = await conn.fetch(
            """
                            WITH vector_matches AS (
                              SELECT product_id, 1 - (embedding <=> $1) AS similarity
                              FROM product_embeddings
                              WHERE 1 - (embedding <=> $1) > $2
                              ORDER BY similarity DESC
                              LIMIT $3
                            )
                            SELECT product_name, list_price, description FROM products
                            WHERE product_id IN (SELECT product_id FROM vector_matches)
                            AND list_price >= $4 AND list_price <= $5
                            """,
            qe,
            similarity_threshold,
            num_matches,
            min_price,
            max_price,
        )

        if len(results) == 0:
            raise Exception("Did not find any results. Adjust the query parameters.")

        for r in results:
            # Collect the description for all the matched similar toy products.
            matches.append({"product": r["product_name"], "price": round(r["list_price"], 2), "description": r["description"]})
            '''
            matches.append(
                f"""The name of the toy is {r["product_name"]}.
                          The price of the toy is ${round(r["list_price"], 2)}.
                          Its description is below:
                          {r["description"]}."""
            )
            '''
        await conn.close()

    return matches

def summary_features(user_query:str, matches: list):
    from langchain.chains.summarize import load_summarize_chain
    from langchain.docstore.document import Document
    from langchain.llms import VertexAI
    from langchain import PromptTemplate

    llm = VertexAI()

    map_prompt_template = """
              You will be given a detailed description of a toy product.
              This description is enclosed in triple backticks (```).
              Using this description only, extract the name of the toy,
              the price of the toy and its features.

              ```{text}```
              SUMMARY:
              """
    map_prompt = PromptTemplate(template=map_prompt_template, input_variables=["text"])

    combine_prompt_template = """
                You will be given a detailed description different toy products
                enclosed in triple backticks (```) and a question enclosed in
                double backticks(``).
                Select one toy that is most relevant to answer the question.
                Using that selected toy description, answer the following
                question in as much detail as possible.
                You should only use the information in the description.
                Your answer should include the name of the toy, the price of the toy
                and its features. Your answer should be less than 200 words.
                Your answer should be in Markdown in a numbered list format.

                Description:
                ```{text}```

                Question:
                ``{user_query}``

                Answer:
                """
    combine_prompt = PromptTemplate(
        template=combine_prompt_template, input_variables=["text", "user_query"]
    )
    docs = []
    for index in range(len(matches)):
        product_info = matches[index]["product"]
        price_info   = matches[index]["price"]
        description  = matches[index]["description"]
        docs.append(Document(page_content = f"The name of the toy is {product_info}. The price of the toy is {price_info}. Its description is below: {description}"))
    #docs = [Document(page_content=t) for t in matches]
    chain = load_summarize_chain(
        llm, chain_type="map_reduce", map_prompt=map_prompt, combine_prompt=combine_prompt
    )
    answer = chain.run(
        {
            "input_documents": docs,
            "user_query": user_query,
        }
    )
    return answer

# if __name__ == '__main__':
#     import time
#     user_query = "Do you have a beach toy set that teaches numbers and letters to kids?"
#     min_price = 20
#     max_price = 100
#     start = time.time()
#     matches = asyncio.run(select_toy_byquery(user_query, min_price, max_price))
#     print(matches)
#     print(f"time: {time.time() - start} (s)")

#     start = time.time()
#     answer = summary_features(matches)
#     print(f"answer: {answer}")
#     print(f"feature_summary_time: {time.time() - start} (s)")