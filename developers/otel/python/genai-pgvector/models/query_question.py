from tortoise import fields
from tortoise.models import Model


class Question(Model):
    customer_query = fields.CharField(max_length=256)
    min_price = fields.CharField(max_length=64)
    max_price = fields.CharField(max_length=64)

    def __str__(self):
        return f"Query: {customer_query}, Min/Max Price: {self.min_price}/{self.max_price}"