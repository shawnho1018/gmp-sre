import time
from typing import Dict, Optional
from fastapi import FastAPI, HTTPException, Request
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.templating import Jinja2Templates
from fastapi.encoders import jsonable_encoder
from genai.query import select_toy_byquery, summary_features
from pydantic import BaseModel
from opentelemetry.instrumentation.fastapi import FastAPIInstrumentor
# For manual tracing
from opentelemetry import trace

# For manual metrics
from opentelemetry.metrics import (
    CallbackOptions,
    Observation,
    get_meter_provider,
    set_meter_provider,
)
from opentelemetry.sdk.metrics import MeterProvider
from opentelemetry.sdk.metrics.export import (
    ConsoleMetricExporter,
    PeriodicExportingMetricReader,
)

description = """
GenAI-PGVector is a shopping assistant which helps you pick the BEST Toy for your kids. 🚀
"""

app = FastAPI(
    title="genai-toystore-shopping-assistant",
    description=description,
    summary="Shopping Assistant for Toy",
    version="0.0.1",
    terms_of_service="http://example.com/terms/",
    contact={
        "email": "shawnho@google.com",
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
)

# FastAPI Auto Instrumentation
FastAPIInstrumentor.instrument_app(app)

# Metrics Exporter
exporter = ConsoleMetricExporter()
reader   = PeriodicExportingMetricReader(exporter)
provider = MeterProvider(metric_readers=[reader])
set_meter_provider(provider)

# Initialize Metrics
meter = get_meter_provider().get_meter("genai-pgvector", "0.1")
query_counter = meter.create_counter("query_call")
query_histogram = meter.create_histogram("query_histogram")
recommendation_counter = meter.create_counter("recommendation_call")
recommendation_histogram = meter.create_histogram("recommendation_histogram")

# [START opentelemetry_prom_exemplars_attach]
def get_prom_exemplars() -> Optional[Dict[str, str]]:
    """Generates an exemplar dictionary from the current implicit OTel context if available"""
    span_context = trace.get_current_span().get_span_context()

    # Only include the exemplar if it is valid and sampled
    if span_context.is_valid and span_context.trace_flags.sampled:
        # You must set the trace_id and span_id exemplar labels like this to link OTel and
        # Prometheus. They must be formatted as hexadecimal strings.
        print(f"trace_id: ${span_context.span_id}, span_id: ${span_context.span_id}")
        return {
            "trace_id": trace.format_trace_id(span_context.trace_id),
            "span_id": trace.format_span_id(span_context.span_id),
        }
    print("None Exemplar in get_prom_exemplars")
    return None


class CreateQueryPayload(BaseModel):
    user_query: str
    min_price: str
    max_price: str

@app.post("/query")
async def query(query: CreateQueryPayload):
    print(f"Post Query: {query}")
    start_time = time.time()
    current_span = trace.get_current_span()
    current_span.set_attribute("user_query", query.user_query)
    current_span.add_event("start toy query")
    print(f"Start Query")
    candidate = await select_toy_byquery(query.user_query, query.min_price, query.max_price)
    print(f"End Query")
    current_span.add_event("finalize toy query")
    json_compatible_item_data = jsonable_encoder(candidate)

    query_counter.add(1)
    query_histogram.record(time.time()-start_time, get_prom_exemplars())
    return JSONResponse(content=json_compatible_item_data)

@app.post("/recommendation")
async def recommend(query: CreateQueryPayload):
    print(f"Post Query: {query}")
    start_time = time.time()
    current_span = trace.get_current_span()
    current_span.set_attribute("user_query", query.user_query)
    current_span.add_event("start toy query")
    print(f"Start Query")
    candidates = await select_toy_byquery(query.user_query, query.min_price, query.max_price)
    current_span.add_event("start summarization")
    print(f"End Query")
    recommendation = summary_features(query.user_query, candidates)
    current_span.add_event("finalize summarization")
    print(f"End Summrization")
    json_compatible_item_data = jsonable_encoder(recommendation)


    recommendation_counter.add(1)
    recommendation_histogram.record(time.time()-start_time, get_prom_exemplars())
    return JSONResponse(content=json_compatible_item_data)

