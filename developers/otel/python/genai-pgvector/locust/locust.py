from locust import HttpUser, task, between, events
import random

class QuickstartUser(HttpUser):

    @task(1)
    def recommendation(self):
        self.client.post(f"/recommendation", json={"user_query": "sand tool for kids to shovel in the sand hole", "min_price": "5", "max_price": "30"})
    @task(1)
    def query(self):
        self.client.post(f"/query", json={"user_query": "sand tool for kids to shovel in the sand hole", "min_price": "5", "max_price": "30"})
