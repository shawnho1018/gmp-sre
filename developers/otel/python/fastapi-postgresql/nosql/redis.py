import asyncio_redis, os
# For manual tracing
from opentelemetry import trace
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor,
    ConsoleSpanExporter,
)

# Setup Manual OTEL Tracker
provider = TracerProvider()
provider.add_span_processor(BatchSpanProcessor(ConsoleSpanExporter()))
trace.set_tracer_provider(provider)
tracer = trace.get_tracer(__name__)

redis_host = '34.80.231.200'
#redis_host = 'redis-cart.redis.svc.cluster.local'


# This will report a span with the default settings
def redis_get(key):
    value = None
    redis_client = yield from asyncio_redis.Pool.create(host=redis_host, port=6379, poolsize=10)
    transaction = yield from connection.multi()
    with tracer.start_as_current_span("redis_get") as span:
        span.add_event("redis_get_start")
        f1 = yield from transaction.set('key', 'value')
        span.add_event("redis_get_done")
    return value

async def redis_set(key, value):
    result = False
    with tracer.start_as_current_span("redis_set") as span:
        span.add_event("redis_set_start")
        await redis_client.set(key, value)
        span.add_event("redis_set_done")

async def redis_keys():
    keys = None
    with tracer.start_as_current_span("redis_get_key") as span:
        span.add_event("redis_get_key_start")
        keys = await redis_client.keys("*")
        span.add_event("redis_get_key_done")
    return keys