import time, os
from typing import Dict, Optional
import redis.asyncio as redis
from fastapi import FastAPI, HTTPException, Request
from fastapi.responses import HTMLResponse, JSONResponse
from fastapi.templating import Jinja2Templates
from fastapi.encoders import jsonable_encoder
from tortoise.functions import Count
from database import init_db
from pydantic import BaseModel
from models.model import Note, Vote
from settings import settings
from opentelemetry.instrumentation.fastapi import FastAPIInstrumentor
# For manual tracing
from opentelemetry import trace
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor,
    ConsoleSpanExporter,
)

# For manual metrics
from opentelemetry.sdk.metrics import MeterProvider
from opentelemetry.sdk.metrics.export import (
    ConsoleMetricExporter,
    PeriodicExportingMetricReader,
)
from opentelemetry.metrics import (
    CallbackOptions,
    Observation,
    get_meter_provider,
    set_meter_provider,
)

# FastAPI initialization
description = """
FastAPI with Postgresql is a voting system to determine either Space or Tab team wins the race. 🚀
"""
redis_host = '34.80.231.200'
if os.getenv("RUNTIME_ENV") == "GKE":
    redis_host = "redis-cart.redis.svc.cluster.local"

app = FastAPI(
    title="fastapi-postgresql",
    description=description,
    summary="Cool voting software by NYT",
    version="0.0.1",
    terms_of_service="http://example.com/terms/",
    contact={
        "email": "shawnho@google.com",
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
)
# FastAPU Auto Instrumentation
FastAPIInstrumentor.instrument_app(app)

# Setup Manual OTEL Tracker
provider = TracerProvider()
provider.add_span_processor(BatchSpanProcessor(ConsoleSpanExporter()))
trace.set_tracer_provider(provider)
tracer = trace.get_tracer(__name__)

# Set up the Jinja template
templates = Jinja2Templates(directory="templates")
init_db(app)
print(f"settings: {settings.DB_NAME}")

# Metrics Exporter
exporter = ConsoleMetricExporter()
reader   = PeriodicExportingMetricReader(exporter)
provider = MeterProvider(metric_readers=[reader])
set_meter_provider(provider)
# Initialize Metrics

meter = get_meter_provider().get_meter("fastapi-server", "0.1")
vote_counter = meter.create_counter("vote_call")
vote_histogram = meter.create_histogram("vote_histogram")
index_counter = meter.create_counter("index_call")
index_histogram = meter.create_histogram("index_histogram")
dice_counter = meter.create_counter("dice_call")
dice_histogram = meter.create_histogram("dice_histogram")
roll_counter = meter.create_counter("roll_call")
roll_histogram = meter.create_histogram("roll_histogram")
note_counter = meter.create_counter("note_call")
note_histogram = meter.create_histogram("note_histogram")

class Status(BaseModel):
    message: str

# [START opentelemetry_prom_exemplars_attach]
def get_prom_exemplars() -> Optional[Dict[str, str]]:
    """Generates an exemplar dictionary from the current implicit OTel context if available"""
    span_context = trace.get_current_span().get_span_context()

    # Only include the exemplar if it is valid and sampled
    if span_context.is_valid and span_context.trace_flags.sampled:
        # You must set the trace_id and span_id exemplar labels like this to link OTel and
        # Prometheus. They must be formatted as hexadecimal strings.
        exemplar = {
            "trace_id": trace.format_trace_id(span_context.trace_id),
            "span_id": trace.format_span_id(span_context.span_id),
        }
        
        print(f"${exemplar}")
        return exemplar
    print("None Exemplar in get_prom_exemplars")
    return None

@app.get("/", response_class=HTMLResponse)
async def index(request: Request):
    index_counter.add(1)
    start_time = time.time()
    ret = await Vote.annotate(count=Count("id")).group_by("team").values("team", "count")
    tab_count = 0
    space_count = 0
    if len(ret) == 1:
        if ret[0]['team'] == "TABS":
            tab_count = ret[0]['count']
        elif ret[0]['team'] == "SPACES":
            space_count = ret[0]['count']
    elif len(ret) == 2:
        if ret[0]['team'] == "TABS":
            tab_count = ret[0]['count']
            space_count = ret[1]['count']
        elif ret[0]['team'] == "SPACES":
            tab_count = ret[1]['count']
            space_count = ret[0]['count']
    index_histogram.record(time.time() - start_time)
    return templates.TemplateResponse("index.html", {"request": request, "space_count": space_count, "tab_count": tab_count})

@app.get("/healthy", response_class=HTMLResponse)
def healthcheck():
    return "<h1>All good!</h1>"

class CreateVotePayload(BaseModel):
    team: str

@app.post("/votes")
async def create_vote(payload: CreateVotePayload):
    """Processes a single vote from user."""
    print(payload.dict())
    vote_counter.add(1)
    start_time = time.time()
    vote = await Vote.create(**payload.dict())
    vote_histogram.record(time.time() - start_time)
    return {"message": f"Vote created successfully with id {vote.id}"}

class CreateNotePayload(BaseModel):
    filename: str
    title: str
    content: str

class CreateDicePayload(BaseModel):
    facet: str

@app.post("/title/create")
async def create_note(payload: CreateNotePayload):
    note_counter.add(1)
    start_time = time.time()
    note = await Note.create(**payload.dict())
    note_histogram.record(time.time() - start_time)
    return {"message": f"Note created successfully with id {note.id}"}

@app.get("/titles")
async def get_note():
    note_counter.add(1)
    start_time = time.time()
    notes = await Note.all()
    json_compatible_item_data = jsonable_encoder(notes)
    note_histogram.record(time.time() - start_time)
    return JSONResponse(content=json_compatible_item_data)

@app.delete("/title/{title_id}")
async def delete_title(title_id: str):
    deleted_note = await Note.filter(title=title_id).delete()
    if not deleted_note:
        raise HTTPException(status_code=404, detail=f"Title {title_id} not found")
    return Status(message=f"Deleted title {title_id}")

@app.get("/title/{title_id}")
async def get_title(title_id: str):
    note_counter.add(1)
    start_time = time.time()
    if not (note := await Note.get_or_none(title=title_id)):
        raise HTTPException(status_code=404, detail="Note not found")
    json_compatible_item_data = jsonable_encoder(note)
    note_histogram.record(time.time() - start_time)
    return JSONResponse(content=json_compatible_item_data)

@app.post("/dice")
async def dice(facet: CreateDicePayload):
    print(f"facet: {facet.facet}")
    dice_counter.add(1)
    start_time = time.time()
    r = await redis.from_url(f"redis://{redis_host}")
    span = trace.get_current_span()
    span.add_event("redis_set_start")
    async with r.pipeline(transaction=True) as pipe:
        result = await (pipe.set("facet", facet.facet).execute())

    span.add_event("redis_set_done")
    span.set_attribute(key="facet", value=facet.facet)
    dice_histogram.record(time.time() - start_time)
    return {"facet": facet.facet}

@app.get("/roll")
async def roll():
    import random
    roll_counter.add(1)
    start_time = time.time()
    r = await redis.from_url(f"redis://{redis_host}")
    async with r.pipeline(transaction=True) as pipe:
        with tracer.start_as_current_span("redis_get") as span:
            span.add_event("redis_get_start")
            point = await (pipe.get("facet").execute())
            if len(point) == 1:
                result = random.randint(1, int(point.pop()))
            span.add_event("redis_get_done")

    roll_histogram.record(time.time() - start_time)
    return {"roll": result}