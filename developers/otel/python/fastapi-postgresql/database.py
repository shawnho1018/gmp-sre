from fastapi import FastAPI
from tortoise import Tortoise
from tortoise.contrib.fastapi import register_tortoise
import os
from settings import settings

Tortoise.init_models(["models.model"], "models")

TORTOISE_ORM = {
    "connections": {
        "default": f"postgres://postgres:postgres@127.0.0.1:5432/orderdb",
        #f"postgres://{settings.DB_USER}:{settings.DB_PASS}@{settings.DB_HOST}:{settings.DB_PORT}/{settings.DB_NAME}",
    },
    "apps": {
        "models": {
            "models": ["models.model", "aerich.models"],
            "default_connection": "default",
        },
    },
    "use_tz": False,
}


def init_db(app: FastAPI) -> None:
    register_tortoise(
        app,
        config=TORTOISE_ORM,
        modules={"models": ["models.model", "aerich.models"]},
        generate_schemas=True,
        add_exception_handlers=True,
    )