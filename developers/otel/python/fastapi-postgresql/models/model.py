from tortoise import fields
from tortoise.models import Model


class Note(Model):
    id = fields.IntField(pk=True)
    created_at = fields.DatetimeField(auto_now_add=True)
    updated_at = fields.DatetimeField(auto_now=True)
    filename = fields.CharField(max_length=256)
    title = fields.CharField(max_length=1000)
    content = fields.TextField(default="")
    def __str__(self):
        return self.title

class Candidate(Model):
    name = fields.CharField(max_length=256)
    def __str__(self):
        return self.name


class Vote(Model):
    id = fields.IntField(pk=True)
    created_at = fields.DatetimeField(auto_now_add=True)
    team = fields.CharField(max_length=256)
    #team = fields.ForeignKeyRelation[Candidate] = fields.ForeignKeyField("models.Candidate", related_name="votes")
    def __str__(self):
        return self.team
    
class Dice(Model):
    facet = fields.CharField(max_length=64)