# FastAPI Postgresql

## Application Technical Stack
This project demonstrates a voting system. The technical stack behinds consists of [FastAPI](https://fastapi.tiangolo.com/tutorial/metadata/) for Restful framework, [Tortoise ORM](https://tortoise.github.io/query.html) for DB object relational mapping, [aerich](https://github.com/tortoise/aerich) for database migration and [cloud-sql-proxy](https://github.com/GoogleCloudPlatform/cloud-sql-proxy) for db proxying. UI adopts from [Material CSS Website](https://materializecss.com/).

| Module  | Usage  | How to utilize it?  |
|---|---|---|
| config.env.sample  | Configurable DB Setting  | First, cat config.env.sample \| base64. Then, paste the result to data.config.env inside secret.yaml  |
| database.py | Define aerich procedure | Used by aerich tool |
| settings.py  | DB Setting | DB migration is required. Using aerich init -t database.TORTOISE_ORM, aerich init-db, aerich migrate, and aerich update.  |
| model/models.py  | Data Model of the Object  | ```__str__``` defined how to print the object  |
| main.py | FastAPI implementation & TORTOISE ORM query demo | Two services: (1) voting service for TABS/SPACES and (2) Document Archiving Service | 

cloud-sql-proxy is deployed as sidecar to proxy to Postgresql DB, mainly for production, with the adoption of PostgreSQL in CloudSQL. Docker-based Postgresql container is used for test env and the deployment command is shown below.
```
docker run --name postgres -e POSTGRES_PASSWORD=postgres --rm -p 5432:5432 postgres
``` 

An environmental variable, RUNTIME_ENV is used to decide the correct econfiguration file. This configuration is provided as secret in GKE and mounted in /config folder.
```
path = Path(__file__).parent
if os.getenv("RUNTIME_ENV") == "GKE":
    path = "/config"
settings = Settings(_env_file=f"{path}/config.env", _env_file_encoding="utf-8")
```

We also enable cloud-sql-proxy's prometheus metrics exporting capabilities by assigning the running parameters:
```
- "--prometheus"
- "--http-address=0.0.0.0"
- "--http-port=9090"
```

## Monitoring Technical Stack
To further demonstrate the monitoring capabilities, we utilize [opentelemetry-fastapi](https://opentelemetry-python-contrib.readthedocs.io/en/latest/instrumentation/fastapi/fastapi.html) to plugin into the application. The working mechanism is shown below:
<img src="images/otel-gke.png" alt="drawing" width="800"/>

### Deploy Opentelemetry Operator + Collector
Refer [this document](https://cloud.google.com/blog/topics/developers-practitioners/easy-telemetry-instrumentation-gke-opentelemetry-operator/) to deploy Otel Operator & Collector into your GKE. 

### Apply Programming Instrumentation into Code
Programming instrumentation mechanism is adopted in this project. To properly use this, it requires:
* Plugin this part: FastAPIInstrumentor.instrument_app(app), into the main.py.
* Provide [Instrumentation YAML](kubernetes-manifests/instrumentation.yaml) into GKE.
* Add the following annotations into your deployment YAML:
```
sidecar.opentelemetry.io/inject: "true"
instrumentation.opentelemetry.io/inject-python: "true"
```



