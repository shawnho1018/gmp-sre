from pydantic_settings import BaseSettings
from pathlib import Path
import os

class Settings(BaseSettings):
    DB_NAME: str
    DB_HOST: str
    DB_PORT: int
    DB_USER: str
    DB_PASS: str

path = Path(__file__).parent
if os.getenv("RUNTIME_ENV") == "GKE":
    path = "/config"
settings = Settings(_env_file=f"{path}/config.env", _env_file_encoding="utf-8")