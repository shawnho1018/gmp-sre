from locust import HttpUser, task, between, events
import random

class QuickstartUser(HttpUser):
    wait_time = between(1, 5)

    @task(1)
    def roll(self):
        self.client.get("/roll")
    @task(2)
    def title(self):
        number = random.randint(1,5)
        self.client.get(f"/title/note_{number}")
    @task(1)
    def titles(self):
        self.client.get("/titles")

    def on_start(self):
      self.client.post("/dice", json={"facet": "20"})
