# Copyright The OpenTelemetry Authors
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import time, datetime
from random import randint
from sys import argv
from os import environ
from requests import get

from opentelemetry import trace
from opentelemetry.propagate import inject
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.resources import Resource
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor,
    ConsoleSpanExporter,
)

resource = Resource.create(
    {
        "service.name": "client-server",
        "service.namespace": "default",
        "service.instance.id": "python-client",
    }
)

tracer_provider = TracerProvider(resource=resource)

trace.set_tracer_provider(tracer_provider)
tracer = trace.get_tracer_provider().get_tracer(__name__)

trace.get_tracer_provider().add_span_processor(
    BatchSpanProcessor(ConsoleSpanExporter())
)

# Use OS pylib to get environmental variable SERVER_IP
def request(server, type):
    with tracer.start_as_current_span("client"):
        if type == 1:
            with tracer.start_as_current_span("make-request"):
                headers = {}
                inject(headers)
                current_time = datetime.datetime.now()
                requested = get(
                    f"http://{server}/server_request",
                    params={"param": current_time.strftime('%H:%M:%S')},
                    headers=headers,
                )
        elif type == 2:
            with tracer.start_as_current_span("roll-dice"):
                headers = {}
                inject(headers)
                current_time = datetime.datetime.now()
                requested = get(
                    f"http://{server}/roll",
                    headers=headers,
                )
            assert requested.status_code == 200

if __name__ == '__main__':
    # write a while loop which periodically wakes up every 2 seconds and call request function
    # with server ip as argument
    while True:
        res = randint(1, 2)
        request("python-server", res)
        # sleep for 2 seconds
        time.sleep(2)

#    # write a while loop which periodically wakes up every 2 seconds and call request function
#    # with server ip as argument