import os, redis, asyncio
from signal import signal, SIGINT
from opentelemetry import metrics
from random import randint
from flask import Flask, render_template, request

from opentelemetry.instrumentation.wsgi import collect_request_attributes
from opentelemetry.instrumentation.redis import RedisInstrumentor
from opentelemetry.propagate import extract
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import (
    BatchSpanProcessor,
    ConsoleSpanExporter,
)
from opentelemetry.trace import (
    SpanKind,
    get_tracer_provider,
    set_tracer_provider,
)
from logging.config import dictConfig

dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://sys.stdout',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})


def handler(signal_received, frame):
    # SIGINT or  ctrl-C detected, exit without error
    exit(0)

app = Flask(__name__)

# Instrument redis
RedisInstrumentor().instrument()
redis_host = '34.80.231.200'
#redis_host = 'redis-cart.redis.svc.cluster.local'
pool = redis.ConnectionPool(host=redis_host, port=6379, db=0)
redis_client = redis.Redis(connection_pool=pool)

set_tracer_provider(TracerProvider())
tracer = get_tracer_provider().get_tracer(__name__)

get_tracer_provider().add_span_processor(
    BatchSpanProcessor(ConsoleSpanExporter())
)


@app.route("/server_request")
def server_request():
    with tracer.start_as_current_span(
        "server_request",
        context=extract(request.headers),
        kind=SpanKind.SERVER,
        attributes=collect_request_attributes(request.environ),
    ):
        app.logger.info('Get request param %s successfully', request.args.get("param"))
        return "served"

@app.route('/roll')
def roll_dice():
    with tracer.start_as_current_span(
        "roll-manual",
        context=extract(request.headers),
        kind=SpanKind.SERVER,
        attributes=collect_request_attributes(request.environ),
    ):
        result = str(do_roll())
        app.logger.info(f'Roll dice {result}')
    return result

def do_roll():
    with tracer.start_as_current_span(
        "rolling",
        context=extract(request.headers),
        kind=SpanKind.SERVER,
        attributes=collect_request_attributes(request.environ),
    ):
        res = randint(1, 6)
        update_dice(res)
        
    return res
def update_dice(number):
    count = redis_client.get(f"dice-{number}")
    if count is not None:
        count = int(count) + 1
    else:
        count = 0
    redis_client.set(f"dice-{number}", count)

@app.route("/")
def index():
    return render_template('web.html')
@app.route("/save", methods=['POST'])
def save():
    field = request.form['field']
    value = request.form['value']
    ret = redis_client.set(field, value)
    app.logger.debug(ret)
    new_value = redis_client.get(field)
    return render_template('web.html', saved=1, value=new_value)

@app.route("/get", methods=['POST'])
def get():
    field = request.form['field']
    value = redis_client.get(field)
    if value is None:
        return render_template('web.html', field=field, value="Not defined yet")
    str_value = value.decode('utf-8')
    return render_template('web.html', field=field, value=str_value)

@app.route("/keys", methods=['GET'])
def keys():
    all_keys = redis_client.keys("*")
    return render_template('web.html', fields=all_keys)



if __name__ == '__main__':
    signal(SIGINT, handler)
    server_port = os.environ.get('PORT', '8080')

    app.run(debug=False, use_reloader=False, port=server_port, host='0.0.0.0')