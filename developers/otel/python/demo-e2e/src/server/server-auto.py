import os
from signal import signal, SIGINT
from opentelemetry import metrics
from random import randint
from flask import Flask, render_template, request
from logging.config import dictConfig

# Use dictConfig to set flask logging from stderr to stdout
dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://sys.stdout',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})


def handler(signal_received, frame):
    # SIGINT or  ctrl-C detected, exit without error
    exit(0)

app = Flask(__name__)
# Acquire a meter.
meter = metrics.get_meter("diceroller.meter")
# Now create a counter instrument to make measurements with
roll_counter = meter.create_counter(
    "roll_counter",
    description="The number of rolls by roll value",
)
@app.route("/server_request")
def server_request():
    app.logger.info('Get request param %s successfully', request.args.get("param"))
    return "served"

@app.route('/roll')
def roll_dice():
    result = str(do_roll())
    app.logger.info(f'Roll dice {result}')
    return result

def do_roll():
    res = randint(1, 6)
    return res

if __name__ == '__main__':
    signal(SIGINT, handler)
    server_port = os.environ.get('PORT', '8080')
    app.run(debug=False, use_reloader=False, port=server_port, host='0.0.0.0')