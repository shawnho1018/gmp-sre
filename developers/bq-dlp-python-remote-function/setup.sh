#!/bin/bash
CONNECTION_NAME="my_gcf_conn"
LOCATION="asia-east1"
REMOTE_FUNCTION_NAME="addFakeUser"
FUNCTION_NAME="addfakeuser"
PROJECT_ID=$(gcloud config get-value project)
DATASET="blogs"
RUN_URL="https://addfakeuser-fr4elfpuza-de.a.run.app"

bq mk --connection --display_name="${CONNECTION_NAME}" \
      --connection_type=CLOUD_RESOURCE \
      --project_id="${PROJECT_ID}" \
      --location="${LOCATION}" "${CONNECTION_NAME}" 
bq show --location="${LOCATION}" --connection "${CONNECTION_NAME}"

# Create BigQuery DataSet
bq mk --location "${LOCATION}" -d "${DATASET}" && bq show "${DATASET}"

gcloud functions deploy "${FUNCTION_NAME}" \
--gen2 \
--runtime=python39 \
--region=asia-east1 \
--source=. \
--entry-point="${REMOTE_FUNCTION_NAME}" \
--trigger-http

echo "SQL Remote Functions"

bq query --project_id "${PROJECT_ID}" --use_legacy_sql=false "CREATE OR REPLACE FUNCTION ${DATASET}.${REMOTE_FUNCTION_NAME}(user_id int64, corp_id STRING, phone_number STRING) RETURNS STRING
REMOTE WITH CONNECTION \`${PROJECT_ID}.${LOCATION}.${CONNECTION_NAME}\`
OPTIONS (endpoint = '${RUN_URL}');"
