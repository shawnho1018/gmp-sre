# BigQuery Remote Function
This function served as remote function in Bigquery. It could use DLP to de-identify user's phone number. A sample BigQuery can be viewed below:
```
WITH data AS (
    SELECT
        number, corp, blogs.addFakeUser(number, corp, phone_number) as reply
    FROM UNNEST(
        ARRAY<STRUCT<number INT64, corp STRING, phone_number STRING>>[
            (1, 'google', '+886920201322'), (2, 'gitlab', '+861357976681'), (3, '17live', '+886927123456')
        ]
    )
)
    
SELECT * EXCEPT(reply),
    JSON_QUERY(reply, '$.email') AS user_id,
    JSON_QUERY(reply, '$.phone') AS masked_number,
    FROM data
```
The expected results could be found as
```
[{
  "number": "1",
  "corp": "google",
  "user_id": "\"user_1@google.com\"",
  "masked_number": "\"**********322\""
}, {
  "number": "2",
  "corp": "gitlab",
  "user_id": "\"user_2@gitlab.com\"",
  "masked_number": "\"**********681\""
}, {
  "number": "3",
  "corp": "17live",
  "user_id": "\"user_3@17live.com\"",
  "masked_number": "\"**********456\""
}]
```