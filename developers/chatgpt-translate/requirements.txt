bs4
openai
requests
ebooklib
cloudevents
Flask
protobuf
rich
python-dotenv
google-cloud-secret-manager
google-cloud-vision==3.1.0
google-cloud-logging==3.5.0
google-cloud-storage==2.0.0; python_version < '3.7'
google-cloud-storage==2.1.0; python_version > '3.6'