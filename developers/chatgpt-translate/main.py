from make_book import BEPUB, ChatGPT, GPT3
from google.cloud import secretmanager, storage
from pathlib import Path
from flask import Flask, request, render_template
from cloudevents.http import from_http
from dotenv import load_dotenv
import google.cloud.logging
import logging, os
app = Flask(__name__)

load_dotenv(dotenv_path=Path("vars.env"))

def _download_file_from_gcs(bucket_name, source_blob_name, dst_file_name):
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(dst_file_name)

def _upload_file_to_gcs(bucket_name, source_file_name, destination_blob_name):
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(destination_blob_name)
    blob.upload_from_filename(source_file_name)

def _get_gcp_secret_(project_number, secret_name, version):
    client = secretmanager.SecretManagerServiceClient()
    secret_resource_name = f"projects/{project_number}/secrets/{secret_name}/versions/{version}"
    response = client.access_secret_version(request={"name":secret_resource_name})
    payload = response.payload.data.decode("UTF-8")
    return payload

# create an endpoint at http://localhost:/3000/
@app.route("/", methods=["GET"])
def health():
    # you can access cloudevent fields as seen below
    logging.info(
        f"get health check"
    )
    #return render_template('index.html', language = "TEST Languge")
    return "SUCCESS", 200
@app.route("/", methods=["POST"])
def home():
    # create a CloudEvent
    
    event = from_http(request.headers, request.get_data())

    # you can access cloudevent fields as seen below
    logging.info(
        f"Found {event['id']} from {event['source']} with type "
        f"{event['type']} and specversion {event['specversion']}"
    )
    r_json = request.get_json()
    if event['source'] == f"//storage.googleapis.com/projects/_/buckets/{os.getenv('SRC_BUCKET_NAME')}":
        # we use model: chatgpt, language: zh-hant (繁體中文)
        try:
            api_key = _get_gcp_secret_(f"{os.getenv('PROJECT_NUMBER')}", f"{os.getenv('OPENAI_KEY_NAME')}", "1")
        except Exception as e:
            msg = (
                f"Retrieve Secret from Project Failed"
            )
            print(f"error: {e}")
            return f"Bad Request: {msg}", 503
        
        options = {
            'model': "chatgpt",
            'language': "zh-hant",
            'resume': False,
        }

        MODEL_DICT = {"gpt3": GPT3, "chatgpt": ChatGPT}
        model = MODEL_DICT.get(options['model'], "chatgpt")
        src_bucket_name = r_json['bucket']
        blob_name = r_json['name']
        prefix_name = blob_name.split(".")[0]
        dst_blob_name = f"{prefix_name}_bilingual.epub"
        logging.info(f"Downloading Blob {blob_name} from GCS {src_bucket_name}")
        try:
            _download_file_from_gcs(src_bucket_name, blob_name, blob_name)
        except Exception as e:
            msg = (
                f"Download file {blob_name} from {src_bucket_name} failed"
            )
            print(f"error: {e}")
            return f"Bad Request: {msg}", 503

        if os.path.exists(blob_name):
            logging.info(f"Call {options['model']} to translate")
            try:
                e = BEPUB(blob_name, model, api_key, options['resume'], language=options['language'])
                e.make_bilingual_book()
            except Exception as e:
                msg = (
                    f"Translation to {options['language']} failed"
                )
                print(f"error: {e}")
                return f"Bad Request: {msg}", 503
        else:
            logging.error(f"Blob File {blob_name} does not exist")
        if os.path.exists(dst_blob_name):
            logging.info(f"Uploading {dst_blob_name} to GCS bucket {os.getenv('DST_BUCKET_NAME')}")
            try:
                _upload_file_to_gcs(os.getenv('DST_BUCKET_NAME'), dst_blob_name, dst_blob_name)
            except Exception as e:
                msg = (
                    f"Upload {dst_blob_name} to GCS bucket failed"
                )
                print(f"error: {e}")
                return f"Bad Request: {msg}", 503
        else:
            logging.error(f"Translated File {dst_blob_name} does not exist")
    resp = f"Success! ID: {request.headers.get('ce-id')}"
    return (resp, 200)

if __name__ == "__main__":
    # Instantiates a client
    client = google.cloud.logging.Client()

    # Retrieves a Cloud Logging handler based on the environment
    # you're running in and integrates the handler with the
    # Python logging module. By default this captures all logs
    # at INFO level and higher
    client.setup_logging()    
    server_port = 8080
    app.run(debug=False, port=server_port, host='0.0.0.0')
