# ChatGPT with EventArc+CloudRun
這個專案是為了展示如何使用Event-Driven架構，進行服務的優化。展示情境是通過上傳一份epub的文件到CloudStorage，通過EventArc喚起CloudRun服務，來進行ChatGPT的呼叫，並將翻譯結果儲存在另一個Cloud Storage供客戶取用。

本專案主要透過打包一層EventArc、CloudStorage、與Secret Manager的Wrapper機制，包裹原本以Python commandline執行的chatgpt功能，轉換成為Event-Driven的呼叫架構。下方架構圖提供了整個程式的流程說明。
![架構圖](images/chatgpt-arch.png)

## 使用方法：
這個專案部署分為兩段：
### 程式部署：
主要將Python服務打包並發布於Cloud Run。主要部署步驟已撰寫於Cloudbuild.yaml
```
gcloud builds submit . --substitutions=_IMAGE_TAG="v1.0"
```
### 設定Event-Driven on GCP:
1. 修改對應setup.sh裡的參數
```
export PROJECT_ID="shawnho-demo-2023"
export LOCATION="asia-east1"
export SOURCE_BUCKET="${PROJECT_ID}-image-process"
export CLOUDRUN_SERVICE="translator"
export TRIGGER_NAME="translator"
export OPENAI_KEY_PATH="/Users/shawnho/gcp-keys/openai.key"
```
2. 執行setup.sh

## Reference
* [ChatGPT Translate Python 翻譯程式](https://github.com/yihong0618/bilingual_book_maker)
* [cloudEvent Parser](https://github.com/cloudevents/sdk-python/tree/main/cloudevents/http)
