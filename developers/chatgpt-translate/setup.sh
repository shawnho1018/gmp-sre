#!/bin/bash
export PROJECT_ID="shawnho-demo-2023"
export LOCATION="asia-east1"
export SOURCE_BUCKET="${PROJECT_ID}-image-process"
export CLOUDRUN_SERVICE="translator"
export TRIGGER_NAME="translator"
export OPENAI_KEY_PATH="/Users/shawnho/gcp-keys/openai.key"

function __main__ () {
    __create_openai_key__
    __enable_eventarc_api__
    __create_iam_binding__
    __create_trigger__
}
function __create_openai_key__ () {
  gcloud secrets create openai \
    --replication-policy="automatic" --data-file="${OPENAI_KEY_PATH}"
}


function __enable_eventarc_api__ () {
  gcloud services enable logging.googleapis.com \
    eventarc.googleapis.com
}

function __create_iam_binding__ () {
    SERVICE_ACCOUNT="$(gsutil kms serviceaccount -p ${PROJECT_ID})"

    gcloud projects add-iam-policy-binding ${PROJECT_ID} \
    --member="serviceAccount:${SERVICE_ACCOUNT}" \
    --role="roles/pubsub.publisher"    
}
# Remember the trigger has a default timeout of 10s which is too short for translation cloudrun task. 
# Manual config the timeout value to 60s. 
function __create_trigger__ () {
  gcloud eventarc triggers create ${TRIGGER_NAME} \
    --location=${LOCATION} \
    --destination-run-service=${CLOUDRUN_SERVICE} \
    --destination-run-region=${LOCATION} \
    --event-filters="type=google.cloud.storage.object.v1.finalized" \
    --event-filters="bucket=${SOURCE_BUCKET}" \
    --service-account="anthos-admin@${PROJECT_ID}.iam.gserviceaccount.com"
}

__main__ "$@"